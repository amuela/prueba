import unittest
from Coche import Coche

class TestCoche (unittest.TestCase):
    def test_acelerar(self):
        coche01=Coche("azul", "BMW", "M3", "1234 AAA", 90, 25, 12)
        tiempo = 7
        self.assertEqual(coche01.acelerar(), 265)

    def test_frenar(self):
        coche01=Coche("azul", "BMW", "M3", "1234 AAA", 90, 25, 12)
        tiempo = 7
        self.assertEqual(coche01.frenar(), 6)

if __name__ == "__main__":
  unittest.main()
